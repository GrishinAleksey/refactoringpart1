package com.grishin;

import org.springframework.mail.javamail.JavaMailSenderImpl;
import org.springframework.mail.javamail.MimeMessageHelper;

import javax.mail.MessagingException;
import javax.mail.internet.MimeMessage;

public class MailSender {

    private final MimeMessage message;
    private final JavaMailSenderImpl mailSender = new JavaMailSenderImpl();
    private MimeMessageHelper helper;

    public MailSender(String host, String subject) throws MessagingException {
        mailSender.setHost(host);
        helper.setSubject(subject);
        message = mailSender.createMimeMessage();
    }

    public void sendMail(String resultingHtml, String recipients) {
        try {
            helper = new MimeMessageHelper(message, true);
            helper.setTo(recipients);
            helper.setText(resultingHtml, true);
        } catch (MessagingException e) {
            e.printStackTrace();
        }
        mailSender.send(message);
    }
}
