package com.grishin;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.time.LocalDate;

public class StatementPreparer {

    private final PreparedStatement preparedStatement;

    public StatementPreparer(Connection connection) throws SQLException {
        preparedStatement = connection.prepareStatement("select emp.id as emp_id, emp.name as amp_name, sum(salary) as salary from employee emp left join" +
                "salary_payments sp on emp.id = sp.employee_id where emp.department_id = ? and" +
                " sp.date >= ? and sp.date <= ? group by emp.id, emp.name");
    }

    public ResultSet report(String departmentId, LocalDate dateFrom, LocalDate dateTo) throws SQLException {
        preparedStatement.setString(0, departmentId);
        setDate(dateFrom, dateTo);
        return preparedStatement.executeQuery();
    }

    public void setDate(LocalDate dateFrom, LocalDate dateTo) throws SQLException {
        preparedStatement.setDate(1, new java.sql.Date(dateFrom.toEpochDay()));
        preparedStatement.setDate(2, new java.sql.Date(dateTo.toEpochDay()));
    }
}
