package com.grishin;

import javax.mail.MessagingException;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.time.LocalDate;

public class SalaryHtmlReportNotifier {

    private final Connection connection;

    public SalaryHtmlReportNotifier(Connection databaseConnection) {
        this.connection = databaseConnection;
    }

    public void sendReport(String departmentId, LocalDate dateFrom, LocalDate dateTo, String recipients) {
        try {
            StatementPreparer statementPreparer = new StatementPreparer(connection);
            ResultSet resultSet = statementPreparer.report(departmentId, dateFrom, dateTo);
            HtmlHandler htmlHandler = new HtmlHandler(resultSet);
            String resultHtml = htmlHandler.prepare();
            MailSender mailSender = new MailSender("google.com", "message");
            mailSender.sendMail(resultHtml, recipients);
        } catch (SQLException | MessagingException e) {
            e.printStackTrace();
        }
    }
}